function image_wolf = wolf(image, local_window_size)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
    k = 0.5;
    S = std2(image);
    R = 0;
    M = min(image);
    %k = 0.065;
    %k = 0.2;%spesiaal vir Gatos
    
    image_wolf = zeros(size(image, 1), size(image, 2));
    
    %Calculate maximum standard deviation of all local windows throughout the entire image
    for i = 1 : size(image, 1)
                fprintf('        Wolf Thresholding Progress: %d%%\n', fix(i / size(image, 1) * 100));

        if(i - fix(local_window_size / 2) < 1)
            iii = i + fix(local_window_size / 2);
        elseif(i + fix(local_window_size / 2) > size(image, 1))
            iii = i - fix(local_window_size / 2);
        else
            iii = i;
        end;
        for ii = 1 : size(image, 2)
            if(ii - fix(local_window_size / 2) < 1)
                iv = ii + fix(local_window_size / 2);
            elseif(ii + fix(local_window_size / 2) > size(image, 2))
                iv = ii - fix(local_window_size / 2);
            else
                iv = ii;
            end;
            
            local_window = image(iii - fix(local_window_size / 2) : iii + fix(local_window_size / 2), iv - fix(local_window_size / 2) : iv + fix(local_window_size / 2));
            m = mean2(local_window);
            R_temp = std2(local_window);
            
            if(R_temp > R)
                R = R_temp;
            end;
            
        end;
    end;
    %Binarize using values calculated above
    for v = 1 : size(image, 1)
                        fprintf('        Wolf Thresholding Progress: %d%%\n', fix(v / size(image, 1) * 100));

        if(v - fix(local_window_size / 2) < 1)
            vii = v + fix(local_window_size / 2);
        elseif(v + fix(local_window_size / 2) > size(image, 1))
            vii = v - fix(local_window_size / 2);
        else
            vii = v;
        end;
        for vi = 1 : size(image, 2)
            if(vi - fix(local_window_size / 2) < 1)
                viii = vi + fix(local_window_size / 2);
            elseif(vi + fix(local_window_size / 2) > size(image, 2))
                viii = vi - fix(local_window_size / 2);
            else
                viii = vi;
            end;
            
            local_window = image(vii - fix(local_window_size / 2) : vii + fix(local_window_size / 2), viii - fix(local_window_size / 2) : viii + fix(local_window_size / 2));
            m = mean2(local_window);
            t = (1 - k) * m  + k * M + k * (S / R) * (m - M);
            
            if(image(i, ii) < t)
                image_wolf(v, vi) = 0;
            else
                image_wolf(v, vi) = 255;
            end;
        end;
    end;

end

